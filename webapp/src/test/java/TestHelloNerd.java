package com.atlassian.test;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestHelloNerd {
	
	
	@Test (groups = {"batch1", "integration-tests"})
	public void testHasNerdGlasses() throws Exception {
		/////////////////
		// your actual test would go here
		/////////////////
		
		Assert.assertTrue(true);
	}
	
	@Test (groups = {"batch2", "integration-tests"})
	public void testHasNoSocialLife() throws Exception {
		/////////////////
		// your actual test would go here
		/////////////////
		
		Assert.assertTrue(true);
	}
	
	@Test (groups = {"batch2", "integration-tests"})
	public void testPlaysBoardGames() throws Exception {
		/////////////////
		// your actual test would go here
		/////////////////
		
		Assert.assertTrue(true);
	}
	
	@Test (groups = {"batch1", "integration-tests"})
	public void testBelongsToChessClub() throws Exception {
		/////////////////
		// your actual test would go here
		/////////////////
		
		Assert.assertTrue(true);
	}
}
